﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Stylelabs.Providers;

namespace Stylelabs.Repositories
{
    public class Repository<T> : IRepository<T>
    {
        readonly IDocumentDbRepository<T> db;
        public Repository()
        {
            db = new DocumentDbRepository<T>();
        }

        public void SetCollection(string collectionId)
        {
            db.SetCollection(collectionId);
        }

        public IEnumerable<T> GetAllItems()
        {
            var items = db.GetAllItems();
            return items;
        }
        
        
        public IEnumerable<T> GetItems(Expression<Func<T, bool>> predicate)
        {
            var items = db.GetItems(predicate);
            return items;
        }

        public void CreateItemAsync(T item)
        {
           db.CreateItem(item);
        }


        public T GetItem(Expression<Func<T, bool>> predicate)
        {
            var item = (T)db.GetItems(predicate).FirstOrDefault();
            return item;
        }

        public bool UpdateItemAsync(Expression<Func<T, bool>> predicate, T item)
        {
           return db.UpdateItem(predicate, item);
        }

        public bool DeleteItem(Expression<Func<T, bool>> predicate)
        {
            return db.DeleteItem(predicate);
        }

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
