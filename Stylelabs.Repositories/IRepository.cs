﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Stylelabs.Repositories
{
    public interface IRepository<T> : IDisposable
    {
        void SetCollection(string collectionId);
        IEnumerable<T> GetAllItems();
        IEnumerable<T> GetItems(Expression<Func<T, bool>> predicate);
        void CreateItemAsync(T item);
        T GetItem(Expression<Func<T, bool>> predicate);
        bool UpdateItemAsync(Expression<Func<T, bool>> predicate, T item);
        bool DeleteItem(Expression<Func<T, bool>> predicate);
    }
}
