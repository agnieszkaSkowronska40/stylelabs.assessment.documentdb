﻿using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Stylelabs.Recruitment.Framework;
using System;
using Newtonsoft.Json;

namespace Stylelabs.Recruitment.Entities
{

    #region Property class

    public abstract class Property : IDirtyTrackable
    {
        #region Fields

        private PropertyValue _value;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates if this instance is currently tracking changes.
        /// </summary>
        public bool IsTracking { get; private set; }

        /// <summary>
        /// Indicates if this instance is dirty.
        /// </summary>
        public bool IsDirty { get; private set; }

        /// <summary>
        /// Gets or sets the name of the property.
        /// </summary>
        [JsonProperty(PropertyName ="propertyName")]
        public string Name { get; internal set; }

        /// <summary>
        /// Gets the .NET data-type of the property.
        /// </summary>
        [JsonIgnore]
        public abstract Type DataType { get; }

        /// <summary>
        /// Gets the original value as it was before tracking started.
        /// </summary>
        public PropertyValue OriginalValue { get; private set; }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <remarks>
        /// If the property doesn't have a value, <see cref="Property.Value"/> is set to <c>null</c>.
        /// </remarks>
        /// <exception cref="ArgumentException">Thrown when the specified value is not of the expected data-type.</exception>
        public PropertyValue Value
        {
            get { return _value; }
            set
            {
                if (value != null && !DataType.IsInstanceOfType(value.Value))
                    throw new ArgumentException(string.Format("The specified value must be of type '{0}'.",
                                                              DataType.FullName));

                if (Equals(_value, value)) return;

                OnPropertyChanging();

                _value = value;

                OnPropertyChanged();

                if (IsTracking)
                {
                    IsDirty = true;
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs before the value of the property has changed.
        /// </summary>
        public event EventHandler<MemberValueChangeEventArgs> PropertyValueChanging;

        /// <summary>
        /// Occurs after the value of the property has changed.
        /// </summary>
        public event EventHandler<MemberValueChangeEventArgs> PropertyValueChanged;

        #endregion

        #region Event triggers

        /// <summary>
        /// Triggers the <see cref="PropertyValueChanging"/> event.
        /// </summary>
        protected void OnPropertyChanging()
        {
            var copy = PropertyValueChanging;
            if (copy != null)
            {
                copy(this, new MemberValueChangeEventArgs(Name));
            }
        }

        /// <summary>
        /// Triggers the <see cref="PropertyValueChanged"/> event.
        /// </summary>
        protected void OnPropertyChanged()
        {
            var copy = PropertyValueChanged;
            if (copy != null)
            {
                copy(this, new MemberValueChangeEventArgs(Name));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts tracking changes.
        /// </summary>
        public void StartTracking()
        {
            IsTracking = true;

            //Mark as non-dirty
            IsDirty = false;

            //Store original value
            OriginalValue = Value;
        }

        #endregion
    }

    #endregion

    #region Generic property class

    /// <summary>
    /// Generic implementation of <see cref="Property"/>.
    /// </summary>
    /// <typeparam name="T">The .NET data-type that represents the value.</typeparam>
    public class Property<T> : Property
    {
        /// <summary>
        /// Gets the original value as it was before tracking started.
        /// </summary>
        public new PropertyValue<T> OriginalValue
        {
            get
            {
                return base.OriginalValue == null
                           ? null
                           : new PropertyValue<T>((T)base.OriginalValue.Value);
            }
        }

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <remarks>
        /// If the property doesn't have a value, <see cref="Property{T}.Value"/> is set to <c>null</c>.
        /// </remarks>
        public new PropertyValue<T> Value
        {
            get
            {
                return base.Value == null
                           ? null
                           : new PropertyValue<T>((T)base.Value.Value);
            }
            set
            {
                base.Value = value == null
                                 ? null
                                 : new PropertyValue(value.Value); //Automatically makes the property dirty.
            }
        }

        /// <summary>
        /// Gets the .NET data-type of the property.
        /// </summary>
        public override Type DataType
        {
            get { return typeof(T); }
        }
    }

    #endregion

    #region PropertyValue class

    /// <summary>
    /// The value of a property.
    /// </summary>
    public class PropertyValue
    {
        #region Properties

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public object Value { get; protected set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyValue" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public PropertyValue(object value)
        {
            Guard.ArgumentNotNull(value, "value"); //Important! Other parts of the system rely on the value not being null.
            Value = value;
        }

        #endregion

        #region General methods

        public static PropertyValue<TValue> ToValue<TValue>(TValue value)
        {
            return new PropertyValue<TValue>(value);
        }

        #endregion

        #region Equality

        protected bool Equals(PropertyValue other)
        {
            return Value.Equals(other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PropertyValue)obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public static bool operator ==(PropertyValue left, PropertyValue right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(PropertyValue left, PropertyValue right)
        {
            return !Equals(left, right);
        }

        #endregion
    }

    #endregion

    #region PropertyValue<T> class

    /// <summary>
    /// The value of a property.
    /// </summary>
    /// <typeparam name="T">.NET data-type of the actual value.</typeparam>
    public class PropertyValue<T> : PropertyValue
    {
        #region Properties

        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        public new T Value
        {
            get { return (T)base.Value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyValue{T}" /> class.
        /// </summary>
        /// <param name="value">The value.</param>
        public PropertyValue(T value) : base(value)
        {
            Guard.ArgumentNotNull(value, "value");  //Important! Other parts of the system rely on the value not being null.
        }

        #endregion
    }
    #endregion
}
