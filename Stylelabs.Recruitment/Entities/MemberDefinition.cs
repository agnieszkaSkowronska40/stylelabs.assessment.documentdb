﻿using Stylelabs.Recruitment.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Stylelabs.Recruitment.Framework.JSONConverters;

namespace Stylelabs.Recruitment.Entities
{
   // [JsonObject(MemberSerialization = MemberSerialization.OptIn)]
    public abstract class MemberDefinition : IDirtyTrackable
    {
        #region Fields

        private string _name;
        private bool _isDirty;

        #endregion

        #region Public Properties

        /// <summary>
        /// Indicates if this is a new instance.
        /// A new instance has not been stored in the database.
        /// </summary>
        [JsonIgnore]
        public bool IsNew { get; protected set; }

        /// <summary>
        /// Indicates if this instance is currently tracking changes.
        /// </summary>
        [JsonIgnore]
        public bool IsTracking { get; private set; }

        /// <summary>
        /// Indicates if this instance is dirty.
        /// </summary>
        [JsonIgnore]
        public virtual bool IsDirty { get { return _isDirty || Labels.IsDirty; } }

        /// <summary>
        /// Gets or sets the name of the member.
        /// The name is the same in every language and must be unique per member-definition.
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    if (!IsNew)
                        throw new NotSupportedException("Property Name of an existing MemberDefinition cannot be changed.");

                    _name = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Gets or sets the labels of the definition.
        /// The labels can be different in every language and do not have to be globally unique.
        /// The key of the dictionary represents the culture; the value represents the label for the specified culture.
        /// </summary>
       [JsonProperty(PropertyName = "labels")]
	   [JsonConverter(typeof(ConcreteConverter<TrackingDictionary<CultureInfo, string>>))]
        public ITrackingDictionary<CultureInfo, string> Labels { get; internal set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberDefinition" /> class.
        /// </summary>
        protected MemberDefinition()
        {
            Labels = new TrackingDictionary<CultureInfo, string>();

            MarkAsNew();
            StartTrackingInternal();    //Avoid virtual call in constructor.
            MarkAsDirty();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Starts tracking changes.
        /// </summary>
        public virtual void StartTracking()
        {
            StartTrackingInternal();
        }

        /// <summary>
        /// Marks this instance as new.
        /// </summary>
        public void MarkAsNew()
        {
            IsNew = true;
        }

        /// <summary>
        /// Marks this instance as existing.
        /// </summary>
        public void MarkAsExisting()
        {
            IsNew = false;
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// If the entity-definition is tracking dirtyness, it is marked as dirty.
        /// If the entity-definition is not tracking, nothing happens.
        /// </summary>
        protected void MarkAsDirty()
        {
            if (IsTracking)
                _isDirty = true;
        }

        /// <summary>
        /// Marks this instance as non dirty.
        /// </summary>
        protected void MarkAsNonDirty()
        {
            _isDirty = false;
        }

        #endregion

        #region Private methods

        private void StartTrackingInternal()
        {
            IsTracking = true;
            MarkAsNonDirty();
            Labels.StartTracking();
        }

        #endregion
    }
}
