﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System.Globalization;
using Stylelabs.Recruitment.Framework;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using System.Collections.Generic;
using Newtonsoft.Json;
using Stylelabs.Recruitment.Framework.JSONConverters;

namespace Stylelabs.Recruitment.Entities
{
    #region Relation

    /// <summary>
    /// A relation between 2 entities.
    /// </summary>
    public abstract class Relation : IDirtyTrackable
    {
        #region Properties

        /// <summary>
        /// Gets the role of this relation.
        /// </summary>
        public abstract RelationRole Role { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        public abstract bool IsTracking { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public abstract bool IsDirty { get; }

        /// <summary>
        /// Gets or sets the name of the relation.
        /// </summary>
        public string Name { get; internal set; }

        #endregion

        #region Methods

        /// <summary>
        /// Starts tracking changes.
        /// </summary>
        public abstract void StartTracking();

        /// <summary>
        /// Gets the ids of the entities involved in new or dirty links.
        /// </summary>
        /// <returns></returns>
        public abstract IScoredSet<long> GetNewLinks();

        /// <summary>
        /// Gets the ids of entities involved in removed links.
        /// </summary>
        /// <returns></returns>
        public abstract ICollection<long> GetRemovedLinks();

        /// <summary>
        /// Gets the ids of the linked entities.
        /// </summary>
        /// <returns></returns>
        public abstract IList<long> GetLinks();

        /// <summary>
        /// Sets the links of the relation.
        /// </summary>
        /// <param name="links"></param>
        /// <exception cref="ArgumentException">Thrown when the number of specified links is not supported for the relation.</exception>
        public abstract void SetLinks(params long[] links);

        #endregion
    }

    #endregion

    #region ParentToManyChildrenRelation class

    /// <summary>
    /// A parent to child relation where the parent is related to multiple children.
    /// </summary>
    public class ParentToManyChildrenRelation : Relation
    {
        #region Properties

        /// <summary>
        /// Gets the role of this relation.
        /// </summary>
        public override RelationRole Role
        {
            get { return RelationRole.Parent; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        public override bool IsTracking
        {
            get { return ChildIds.IsTracking; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public override bool IsDirty
        {
            get { return ChildIds.IsDirty; }
        }

        /// <summary>
        /// The ids of the related child entities. The ids in this collection are ordered.
        /// </summary>
         [JsonProperty(PropertyName = "childIDs")]
         [JsonConverter(typeof(ConcreteConverter<TrackingList<long>>))]
        public ITrackingList<long> ChildIds { get; internal set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ParentToManyChildrenRelation" /> class.
        /// </summary>
        public ParentToManyChildrenRelation()
        {
            ChildIds = new TrackingList<long>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts tracking dirtyness.
        /// </summary>
        public override void StartTracking()
        {
            ChildIds.StartTracking();
        }

        /// <summary>
        /// Gets the ids of the entities involved in new or dirty links.
        /// </summary>
        /// <returns></returns>
        public override IScoredSet<long> GetNewLinks()
        {
            var result = new ScoredSet<long>();

            foreach (var current in ChildIds.NewItems)
            {
                result.Add(current, ChildIds.IndexOf(current));
            }

            return result;
        }

        /// <summary>
        /// Gets the ids of entities involved in removed links.
        /// </summary>
        /// <returns></returns>
        public override ICollection<long> GetRemovedLinks()
        {
            return ChildIds.RemovedItems;
        }

        /// <summary>
        /// Gets the ids of the linked entities.
        /// </summary>
        /// <returns></returns>
        public override IList<long> GetLinks()
        {
            return ChildIds;
        }

        /// <summary>
        /// Sets the links of the relation.
        /// </summary>
        /// <param name="links"></param>
        public override void SetLinks(params long[] links)
        {
            ChildIds.Clear();

            foreach (var current in links)
            {
                ChildIds.Add(current);
            }
        }

        #endregion
    }

    #endregion

    #region ParentToOneChildRelation class

    /// <summary>
    /// A parent to child relation where the parent is related to one child.
    /// </summary>
    public class ParentToOneChildRelation : Relation
    {
        #region Fields

        private bool _isDirty;
        private bool _isTracking;
        private long? _childId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the role of this relation.
        /// </summary>
        public override RelationRole Role
        {
            get { return RelationRole.Parent; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        public override bool IsTracking { get { return _isTracking; } }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public override bool IsDirty { get { return _isDirty; } }

        /// <summary>
        /// Gets original child id as it was before tracking started.
        /// </summary>
        public long? OriginalChildId
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets the id of the related child-entity.
        /// </summary>
        public long? ChildId
        {
            get { return _childId; }
            set
            {
                if (_childId != value)
                {
                    OnChildIdChanging();

                    _childId = value;

                    OnChildIdChanged();

                    if (IsTracking)
                    {
                        _isDirty = true;
                    }
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs before the child id has been changed.
        /// </summary>
        public event EventHandler<MemberValueChangeEventArgs> ChildIdChanging;

        /// <summary>
        /// Occurs after the child id has been changed.
        /// </summary>
        public event EventHandler<MemberValueChangeEventArgs> ChildIdChanged;

        #endregion

        #region Event triggers

        /// <summary>
        /// Triggers the <see cref="ChildIdChanging"/> event.
        /// </summary>
        protected void OnChildIdChanging()
        {
            var copy = ChildIdChanging;
            if (copy != null)
            {
                copy(this, new MemberValueChangeEventArgs(Name));
            }
        }

        /// <summary>
        /// Triggers the <see cref="ChildIdChanged"/> event.
        /// </summary>
        protected void OnChildIdChanged()
        {
            var copy = ChildIdChanged;
            if (copy != null)
            {
                copy(this, new MemberValueChangeEventArgs(Name));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts tracking dirtyness.
        /// </summary>
        public override void StartTracking()
        {
            _isTracking = true;

            //Mark as non-dirty
            _isDirty = false;

            //Store original child-id
            OriginalChildId = ChildId;
        }

        /// <summary>
        /// Gets the ids of the entities involved in new or dirty links.
        /// </summary>
        /// <returns></returns>
        public override IScoredSet<long> GetNewLinks()
        {
            var result = new ScoredSet<long>();

            if ((ChildId.HasValue && !OriginalChildId.HasValue) ||
                (ChildId.HasValue && OriginalChildId.HasValue && ChildId.Value != OriginalChildId.Value))
            {
                result.Add(ChildId.Value, 0);
            }
            return result;
        }

        /// <summary>
        /// Gets the ids of entities involved in removed links.
        /// </summary>
        /// <returns></returns>
        public override ICollection<long> GetRemovedLinks()
        {
            var result = new List<long>();

            if ((OriginalChildId.HasValue && !ChildId.HasValue) ||
                (OriginalChildId.HasValue && ChildId.HasValue && OriginalChildId.Value != ChildId.Value))
            {
                result.Add(OriginalChildId.Value);
            }
            return result;
        }

        /// <summary>
        /// Gets the ids of the linked entities.
        /// </summary>
        /// <returns></returns>
        public override IList<long> GetLinks()
        {
            var result = new List<long>();
            if (ChildId.HasValue)
                result.Add(ChildId.Value);
            return result;
        }

        /// <summary>
        /// Sets the links of the relation.
        /// </summary>
        /// <param name="links"></param>
        /// <exception cref="CardinalityViolationException">Thrown when the number of specified links is not supported for the relation.</exception>
        public override void SetLinks(params long[] links)
        {
            if (links.Length > 1)
                throw new CardinalityViolationException("A ParentToOneChildRelation can only have one child.");

            if (links.Length <= 0)
                ChildId = null;
            else
                ChildId = links[0];
        }

        #endregion
    }

    #endregion

    #region ChildToOneParentRelation class

    /// <summary>
    /// A child to parent relation where the child only has one parent.
    /// </summary>
    public class ChildToOneParentRelation : Relation
    {
        #region Fields

        private bool _isTracking;
        private bool _isDirty;
        private long? _parentId;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the role of this relation.
        /// </summary>
        public override RelationRole Role
        {
            get { return RelationRole.Child; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        public override bool IsTracking
        {
            get { return _isTracking; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public override bool IsDirty
        {
            get { return _isDirty; }
        }

        /// <summary>
        /// Gets the original parent id as it was before tracking started.
        /// </summary>
        public long? OriginalParentId
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets the id of the parent entity.
        /// </summary>
        public long? ParentId
        {
            get { return _parentId; }
            set
            {
                if (_parentId != value)
                {
                    OnParentIdChanging();

                    _parentId = value;

                    OnParentIdChanged();

                    if (IsTracking)
                    {
                        _isDirty = true;
                    }
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs before the parent id has been changed.
        /// </summary>
        public event EventHandler<MemberValueChangeEventArgs> ParentIdChanging;

        /// <summary>
        /// Occurs after the parent id has been changed.
        /// </summary>
        public event EventHandler<MemberValueChangeEventArgs> ParentIdChanged;

        #endregion

        #region Event triggers

        /// <summary>
        /// Triggers the <see cref="ParentIdChanging"/> event.
        /// </summary>
        protected void OnParentIdChanging()
        {
            var copy = ParentIdChanging;
            if (copy != null)
            {
                copy(this, new MemberValueChangeEventArgs(Name));
            }
        }

        /// <summary>
        /// Triggers the <see cref="ParentIdChanged"/> event.
        /// </summary>
        protected void OnParentIdChanged()
        {
            var copy = ParentIdChanged;
            if (copy != null)
            {
                copy(this, new MemberValueChangeEventArgs(Name));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts tracking dirtyness.
        /// </summary>
        public override void StartTracking()
        {
            _isTracking = true;

            //Mark as non-dirty
            _isDirty = false;

            //Store original parent-id
            OriginalParentId = ParentId;
        }

        /// <summary>
        /// Gets the ids of the entities involved in new or dirty links.
        /// </summary>
        /// <returns></returns>
        public override IScoredSet<long> GetNewLinks()
        {
            var result = new ScoredSet<long>();

            if ((ParentId.HasValue && !OriginalParentId.HasValue) ||
                (ParentId.HasValue && OriginalParentId.HasValue && ParentId.Value != OriginalParentId.Value))
            {
                result.Add(ParentId.Value, 0);
            }
            return result;
        }

        /// <summary>
        /// Gets the ids of entities involved in removed links.
        /// </summary>
        /// <returns></returns>
        public override ICollection<long> GetRemovedLinks()
        {
            var result = new List<long>();

            if ((OriginalParentId.HasValue && !ParentId.HasValue) ||
                (OriginalParentId.HasValue && ParentId.HasValue && OriginalParentId.Value != ParentId.Value))
            {
                result.Add(OriginalParentId.Value);
            }
            return result;
        }

        /// <summary>
        /// Gets the ids of the linked entities.
        /// </summary>
        /// <returns></returns>
        public override IList<long> GetLinks()
        {
            var result = new List<long>();
            if (ParentId.HasValue)
                result.Add(ParentId.Value);
            return result;
        }

        /// <summary>
        /// Sets the links of the relation.
        /// </summary>
        /// <param name="links"></param>
        /// <exception cref="CardinalityViolationException">Thrown when the number of specified links is not supported for the relation.</exception>
        public override void SetLinks(params long[] links)
        {
            if (links.Length > 1)
            {
                throw new CardinalityViolationException("A ChildToOneParentRelation can only have one parent.");
            }

            if (links.Length <= 0)
            {
                ParentId = null;
            }
            else
            {
                ParentId = links[0];
            }
        }

        #endregion
    }

    #endregion

    #region ChildToManyParentsRelation class

    /// <summary>
    /// A child to parent relation where the child has multiple parents.
    /// </summary>
    public class ChildToManyParentsRelation : Relation
    {
        #region Properties

        /// <summary>
        /// Gets the role of this relation.
        /// </summary>
        public override RelationRole Role
        {
            get { return RelationRole.Child; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        public override bool IsTracking
        {
            get { return ParentIds.IsTracking; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public override bool IsDirty
        {
            get { return ParentIds.IsDirty; }
        }

        /// <summary>
        /// Gets the ids of the parent entities.
        /// </summary>
        public ITrackingList<long> ParentIds { get; internal set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChildToManyParentsRelation" /> class.
        /// </summary>
        public ChildToManyParentsRelation()
        {
            ParentIds = new TrackingList<long>();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Starts tracking dirtyness.
        /// </summary>
        public override void StartTracking()
        {
            ParentIds.StartTracking();
        }

        /// <summary>
        /// Gets the ids of the entities involved in new or dirty links.
        /// </summary>
        /// <returns></returns>
        public override IScoredSet<long> GetNewLinks()
        {
            var result = new ScoredSet<long>();

            foreach (var current in ParentIds.NewItems)
            {
                result.Add(current, ParentIds.IndexOf(current));
            }

            return result;
        }

        /// <summary>
        /// Gets the ids of entities involved in removed links.
        /// </summary>
        /// <returns></returns>
        public override ICollection<long> GetRemovedLinks()
        {
            return ParentIds.RemovedItems;
        }

        /// <summary>
        /// Gets the ids of the linked entities.
        /// </summary>
        /// <returns></returns>
        public override IList<long> GetLinks()
        {
            return ParentIds;
        }

        /// <summary>
        /// Sets the links of the relation.
        /// </summary>
        /// <param name="links"></param>
        public override void SetLinks(params long[] links)
        {
            ParentIds.Clear();

            foreach (var current in links)
            {
                ParentIds.Add(current);
            }
        }

        #endregion
    }

    #endregion

}
