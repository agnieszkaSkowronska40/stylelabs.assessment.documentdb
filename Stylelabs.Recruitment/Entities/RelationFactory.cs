﻿using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Stylelabs.Recruitment.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Stylelabs.Recruitment.Entities
{
    public class RelationFactory : IRelationFactory
    {
        #region Public methods

        /// <summary>
        /// Creates an instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />.
        /// </summary>
        /// <param name="definition">The definition to create a relation for.</param>
        /// <param name="links"></param>
        /// <returns>
        /// An instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />.
        /// </returns>
        /// <exception cref="System.NotSupportedException">Thrown when the definition has a cardinality that is not supported.</exception>
        public Relation Create(RelationDefinition definition, IEnumerable<long> links = null )
        {
            Guard.ArgumentNotNull(definition, "definition");
            return links == null ? Create(definition, new List<long>()) : Create(definition, links);
        }

        /// <summary>
        /// Creates an instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />
        /// and populates the links of the relation with the specified links.
        /// </summary>
        /// <param name="definition">The definition to create a relation for.</param>
        /// <param name="links">The links to populate.</param>
        /// <returns>
        /// An instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />.
        /// </returns>
        private Relation Create(RelationDefinition definition, IList<long> links)
        {
            Guard.ArgumentNotNull(definition, "definition");

            Relation relation;
            if (definition.Role == RelationRole.Parent)
            {
                relation = CreateParentToChildRelation(definition, links);
            }
            else
            {
                relation = CreateChildToParentRelation(definition, links);
            }

            relation.Name = definition.Name;
            return relation;
        }

        #endregion

        #region Private methods

        private static Relation CreateChildToParentRelation(RelationDefinition definition, IList<long> links)
        {
            Guard.ArgumentNotNull(definition, "definition");
            Guard.ArgumentNotNull(links, "links");

            Relation relation;
            switch (definition.Cardinality)
            {
                case RelationCardinality.OneToMany:
                case RelationCardinality.OneToOne:
                    var childToOneParentRel = new ChildToOneParentRelation();

                    if (links.Count == 1)
                        childToOneParentRel.ParentId = links[0];
                    else if (links.Count > 1)
                        throw new ArgumentException("Only one link id can be specified for child to one parent relations.", "links");

                    relation = childToOneParentRel;
                    break;
                case RelationCardinality.ManyToMany:
                    var childToManyParentsRel = new ChildToManyParentsRelation();

                    if (links.Count > 0)
                        childToManyParentsRel.ParentIds = new TrackingList<long>(links.ToList());   //ToList must be called because otherwise, the collection will be of a fixed length.

                    relation = childToManyParentsRel;
                    break;
                default:
                    throw new NotSupportedException(
                        string.Format(
                            "Cardinality '{0}' is not supported",
                            definition.Cardinality));
            }
            return relation;
        }

        private static Relation CreateParentToChildRelation(RelationDefinition definition, IList<long> links)
        {
            Guard.ArgumentNotNull(definition, "definition");
            Guard.ArgumentNotNull(links, "links");

            Relation relation;
            switch (definition.Cardinality)
            {
                case RelationCardinality.OneToMany:
                case RelationCardinality.ManyToMany:
                    var parentToManyChildrenRel = new ParentToManyChildrenRelation();

                    if (links.Count > 0)
                        parentToManyChildrenRel.ChildIds = new TrackingList<long>(links.ToList());   //ToList must be called because otherwise, the collection will be of a fixed length.

                    relation = parentToManyChildrenRel;
                    break;
                case RelationCardinality.OneToOne:
                    var parentToOneChildRel = new ParentToOneChildRelation();

                    if (links.Count == 1)
                        parentToOneChildRel.ChildId = links[0];
                    else if (links.Count > 1)
                        throw new ArgumentException("Only one link id can be specified for parent to one child relations.", "links");

                    relation = parentToOneChildRel;
                    break;
                default:
                    throw new NotSupportedException(
                        string.Format(
                            "Cardinality '{0}' is not supported.",
                            definition.Cardinality));
            }
            return relation;
        }

        #endregion
    }
}
