﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Newtonsoft.Json;
using Stylelabs.Recruitment.Framework;
using Stylelabs.Recruitment.Framework.JSONConverters;

namespace Stylelabs.Recruitment.Entities
{
    public class MemberGroup : IDirtyTrackable
    {
        #region Fields

        private bool _isDirty;
        private string _name;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates if this is a new instance.
        /// A new instance has not been stored in the database.
        /// </summary>
        [JsonIgnore]
        public bool IsNew { get; private set; }

        /// <summary>
        /// Indicates if this instance is currently tracking changes.
        /// </summary>
        [JsonIgnore]
        public bool IsTracking { get; private set; }

        /// <summary>
        /// Indicates if this instance is dirty.
        /// </summary>
        [JsonIgnore]
        public bool IsDirty
        {
            get { return _isDirty || MemberDefinitions.IsDirty || Labels.IsDirty || MemberDefinitions.Any(p => p.IsDirty); }
        }

        /// <summary>
        /// Gets or sets the name of the group.
        /// The name is the same in every language and must be unique per entity definition.
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "memberGroupName")]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    if (!IsNew)
                        throw new NotSupportedException("Property Name of an existing MemberGroup cannot be changed.");

                    _name = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Gets or sets the labels of the group.
        /// The labels can be different in every language and do not have to be globally unique.
        /// The key of the dictionary represents the culture; the value represents the label for the specified culture.
        /// </summary>
        [JsonIgnore]
        public ITrackingDictionary<CultureInfo, string> Labels { get; internal set; }

        /// <summary>
        /// Gets the member definitions that belong to this group.
        /// </summary>
        /// <remarks>
        /// The entity-definition cannot contain multiple members with the same name.
        /// </remarks>
        [ObjectCollectionValidator]
        [JsonConverter(typeof(ConcreteConverter<TrackingList<MemberDefinition>>))]
        public ITrackingList<MemberDefinition> MemberDefinitions { get; internal set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberGroup" /> class.
        /// </summary>
        public MemberGroup()
        {
            MemberDefinitions = new TrackingList<MemberDefinition>();
            Labels = new TrackingDictionary<CultureInfo, string>();

            MarkAsNew();
            StartTracking();
            MarkAsDirty();
        }

        #endregion

        #region Protected methods

        /// <summary>
        /// If the entity-definition is tracking dirtyness, it is marked as dirty.
        /// If the entity-definition is not tracking, nothing happens.
        /// </summary>
        protected void MarkAsDirty()
        {
            if (IsTracking)
            {
                _isDirty = true;
            }
        }

        /// <summary>
        /// Marks this instance as non dirty.
        /// </summary>
        protected void MarkAsNonDirty()
        {
            _isDirty = false;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Starts tracking changes.
        /// </summary>
        public void StartTracking()
        {
            IsTracking = true;
            MarkAsNonDirty();
            Labels.StartTracking();
            MemberDefinitions.StartTracking();

            foreach (var currentMemberDef in MemberDefinitions)
            {
                currentMemberDef.StartTracking();
            }
        }

        /// <summary>
        /// Marks this instance as new.
        /// </summary>
        public void MarkAsNew()
        {
            IsNew = true;

            foreach (var memberDef in MemberDefinitions)
            {
                memberDef.MarkAsNew();
            }
        }

        /// <summary>
        /// Marks this instance as existing.
        /// </summary>
        public void MarkAsExisting()
        {
            IsNew = false;

            foreach (var memberDef in MemberDefinitions)
            {
                memberDef.MarkAsExisting();
            }
        }

        #endregion
    }
}
