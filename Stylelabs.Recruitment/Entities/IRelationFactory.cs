﻿using System.Collections.Generic;

namespace Stylelabs.Recruitment.Entities
{
    /// <summary>
    /// Creates concrete instance of <see cref="Relation"/> based on a specified <see cref="RelationDefinition"/>.
    /// </summary>
    public interface IRelationFactory
    {
        /// Creates an instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />
        /// and populates the links of the relation with the specified links.
        /// <param name="definition">The definition to create a relation for.</param>
        /// <param name="links">The links to populate.</param>
        /// <returns>
        /// An instance of <see cref="Relation" /> for the specified <see cref="RelationDefinition" />.
        /// </returns>
        Relation Create(RelationDefinition definition, IEnumerable<long> links = null);
    }
}
