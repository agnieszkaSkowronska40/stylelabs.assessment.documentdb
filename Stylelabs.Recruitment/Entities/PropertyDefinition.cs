﻿using Stylelabs.Recruitment.Framework;
using System;
using Newtonsoft.Json;

namespace Stylelabs.Recruitment.Entities
{
    #region PropertyDefinition

    /// <summary>
    /// Defines a property that belongs to an entity.
    /// </summary>
    public abstract partial class PropertyDefinition : MemberDefinition, IMandatoryValidatable
    {

        #region Fields

        private bool _multiValue;
        private bool _isMandatory;

        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets a value indicating whether the property can have multiple values.
        /// </summary>
        [JsonIgnore]
        public bool MultiValue
        {
            get { return _multiValue; }
            set
            {
                if (_multiValue != value)
                {
                    if (!IsNew)
                        throw new NotSupportedException("Property MultiValue of an existing PropertyDefinition cannot be changed.");

                    _multiValue = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the property mandatory.
        /// </summary>

        [JsonProperty(PropertyName = "isMandatory")]
        public bool IsMandatory
        {
            get { return _isMandatory; }
            set
            {
                if (_isMandatory != value)
                {
                    _isMandatory = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Indicates if this instance is dirty.
        /// </summary>
        public override bool IsDirty
        {
            get { return base.IsDirty; }
        }

        #endregion

        #region Abstract Properties

        /// <summary>
        /// Gets the data-type of the property.
        /// </summary>
        [JsonIgnore]
        public abstract Type DataType { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyDefinition" /> class.
        /// </summary>
        protected PropertyDefinition()
        {
        }

        #endregion

        #region Overrides

        public override void StartTracking()
        {
            base.StartTracking();
        }

        #endregion
    }

    #endregion

    #region StringPropertyDefinition

    /// <summary>
    /// Implementation of <see cref="PropertyDefinition"/> that contains textual data.
    /// </summary>
    public class StringPropertyDefinition : PropertyDefinition
    {
        #region Fields

        private bool _multiLine;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the data-type of the property.
        /// </summary>
        public override Type DataType
        {
            get { return typeof(string); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the property can contain multiple lines of text.
        /// </summary>
        [JsonIgnore]
        public bool MultiLine
        {
            get { return _multiLine; }
            set
            {
                if (_multiLine != value)
                {
                    _multiLine = value;
                    MarkAsDirty();
                }
            }
        }

        #endregion
    }

    #endregion

    #region DateTimePropertyDefinition

    /// <summary>
    /// Implementation of <see cref="PropertyDefinition"/> that contains dates.
    /// </summary>
    public class DateTimePropertyDefinition : PropertyDefinition
    {
        /// <summary>
        /// Gets the data-type of the property.
        /// </summary>
        public override Type DataType
        {
            get { return typeof(DateTime); }
        }
    }

    #endregion

    #region LongPropertyDefinition

    /// <summary>
    /// Implementation of <see cref="PropertyDefinition"/> that contains 64-bit signed integers.
    /// </summary>
    public class LongPropertyDefinition : PropertyDefinition
    {
        /// <summary>
        /// Gets the data-type of the property.
        /// </summary>
        public override Type DataType
        {
            get { return typeof(long); }
        }
    }

    #endregion
}
