﻿using System.Globalization;

namespace Stylelabs.Recruitment.Entities
{
    /// <summary>
    /// Creates instances of <see cref="Entity"/> populated with the properties and relations defined in a specified <see cref="EntityDefinition"/>.
    /// </summary>
    public interface IEntityFactory
    {
        /// <summary>
        /// Creates a new instance of <see cref="Entity"/>.
        /// The entity will contains the properties defined in the specified <paramref name="definition"/>.
        /// The values of the properties will be stored in the specified <paramref name="culture"/>.
        /// </summary>
        /// <param name="definition">The definition to create an entity for.</param>
        /// <param name="culture">The culture to store the values of the properties in.</param>
        /// <param name="id"></param>
        /// <returns>A new instance of <see cref="Entity"/>.</returns>
        Entity Create(EntityDefinition definition, CultureInfo culture, long id);

        /// <summary>
        /// Creates a new instance of <typeparamref name="T"/>.
        /// The instance will contains the properties defined in the specified <paramref name="definition"/>.
        /// The values of the properties will be stored in the specified <paramref name="culture"/>.
        /// </summary>
        /// <param name="definition">The definition to create a <typeparamref name="T"/> for.</param>
        /// <param name="culture">The culture to store the values of the properties in.</param>
        /// <param name="id"></param>
        /// <returns>A new instance of <typeparamref name="T"/>.</returns>
        T Create<T>(EntityDefinition definition, CultureInfo culture, long id)
            where T : Entity;
    }
}
