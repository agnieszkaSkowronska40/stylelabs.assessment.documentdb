﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System.Globalization;
using Stylelabs.Recruitment.Framework;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Newtonsoft.Json;
using Stylelabs.Recruitment.Framework.JSONConverters;

namespace Stylelabs.Recruitment.Entities
{
    public partial class EntityDefinition : IDirtyTrackable
    {
        #region Fields

        private bool _isDirty;
        private string _name;

        #endregion

        #region Properties

        /// <summary>
        /// Indicates if this is a new instance.
        /// A new instance has not been stored in the database.
        /// </summary>
        [JsonIgnore]
        public bool IsNew { get; private set; }

        /// <summary>
        /// Indicates if this instance is currently tracking changes.
        /// </summary>
        [JsonIgnore]
        public bool IsTracking { get; private set; }

        /// <summary>
        /// Indicates if this instance is dirty.
        /// </summary>
        [JsonIgnore]
        public bool IsDirty
        {
            get { return _isDirty || MemberGroups.IsDirty || Labels.IsDirty || MemberGroups.Any(p => p.IsDirty); }
        }

        /// <summary>
        /// Indicates if this instance is read-only. Changing read-only entity-definitions is not allowed.
        /// </summary>
        [JsonIgnore]
        public bool IsReadOnly { get; internal set; }

        /// <summary>
        /// Gets or sets the unique identifier of the entity definition.
        /// </summary>
        /// <remarks>
        /// When an object has not been made persistent, the Id is set to 0.
        /// </remarks>
        [JsonProperty(PropertyName = "idEntityDef")]
        public long Id { get; internal set; }

        /// <summary>
        /// Gets or sets the name of the definition.
        /// The name is the same in every language and must be globally unique.
        /// </summary>
       
        [Required]
        [JsonProperty(PropertyName = "name")]
        public string Name
        {
            get { return _name; }
            set
            {
                if (_name != value)
                {
                    if (!IsNew)
                        throw new NotSupportedException("Property Name of an existing EntityDefinition cannot be changed.");

                    _name = value;
                    MarkAsDirty();
                }
            }
        }

        /// <summary>
        /// Gets or sets the labels of the definition.
        /// The labels can be different in every language and do not have to be globally unique.
        /// </summary>
        /// <remarks>
        /// The key of the dictionary represents the culture; the value represents the label for the specified culture.
        /// </remarks>
        [JsonProperty(PropertyName = "labels")]
        [JsonConverter(typeof(ConcreteConverter<TrackingDictionary<CultureInfo, string>>))]
        public ITrackingDictionary<CultureInfo, string> Labels { get; internal set; }

        /// <summary>
        /// Gets the member groups that belong to this entity.
        /// </summary>
        /// <remarks>
        /// The entity-definition cannot contain multiple member-groups with the same name.
        /// </remarks>
        [ObjectCollectionValidator]
        [JsonConverter(typeof(ConcreteConverter<TrackingList<MemberGroup>>))]
        public ITrackingList<MemberGroup> MemberGroups { get; set; }

        /// <summary>
        /// Id of the user who last modified this entity-definition.
        /// </summary>
        [JsonIgnore]
        public long ModifiedBy { get; internal set; }

        /// <summary>
        /// Id of the user who created this entity-definition.
        /// </summary>
        [JsonIgnore]
        public long CreatedBy { get; internal set; }

        /// <summary>
        /// The date when this instance was last modified.
        /// </summary>
        [JsonIgnore]
        public DateTime ModifiedOn { get; internal set; }

        /// <summary>
        /// The date when this instance was created on.
        /// </summary>
        [JsonIgnore]
        public DateTime CreatedOn { get; internal set; }

        /// <summary>
        /// The template to use when representing an entity of this definition.
        /// </summary>
        [JsonIgnore]
        public string DisplayTemplate { get; set; }

        #endregion

        #region Constructors

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EntityDefinition()
        {
            MemberGroups = new TrackingList<MemberGroup>();
            Labels = new TrackingDictionary<CultureInfo, string>();

            MarkAsNew();
            // ReSharper disable DoNotCallOverridableMethodsInConstructor
            StartTracking();
            // ReSharper restore DoNotCallOverridableMethodsInConstructor
            MarkAsDirty();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Gets the property definition with the specified name.
        /// If no property-definition with the specified name could be found, <c>null</c> is returned.
        /// </summary>
        /// <param name="name">The name of the property-definition.</param>
        /// <returns>The <see cref="PropertyDefinition"/> with the specified name. If the <see cref="PropertyDefinition"/> could not be found, <c>null</c> is returned.</returns>
        public PropertyDefinition GetPropertyDefinition(string name)
        {
            Guard.ArgumentNotNullOrEmpty(name, "name");

            var memberDef = MemberGroups
                .SelectMany(p => p.MemberDefinitions)
                .FirstOrDefault(p => p.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            return memberDef as PropertyDefinition;
        }

        /// <summary>
        /// Gets a concrete property definition by name.
        /// If no property-definition with the specified name could be found, <c>null</c> is returned.
        /// </summary>
        /// <typeparam name="T">Concrete type of the property definition.</typeparam>
        /// <param name="name">The name of the property-definition.</param>
        /// <returns>
        /// The <see cref="PropertyDefinition" /> with the specified name. If the <see cref="PropertyDefinition" /> could not be found, <c>null</c> is returned.
        /// </returns>
        public T GetPropertyDefinition<T>(string name)
            where T : PropertyDefinition
        {
            return GetPropertyDefinition(name) as T;
        }

        /// <summary>
        /// Gets the relation definition with the specified name.
        /// If no relation-definition with the specified name could be found, <c>null</c> is returned.
        /// </summary>
        /// <param name="name">The name of the relation-definition.</param>
        /// <param name="role">The role of the relation-definition to get.</param>
        /// <returns>
        /// The <see cref="RelationDefinition" /> with the specified name. If the <see cref="RelationDefinition" /> could not be found, <c>null</c> is returned.
        /// </returns>
        public RelationDefinition GetRelationDefinition(string name, RelationRole role)
        {
            Guard.ArgumentNotNullOrEmpty(name, "name");

            var memberDefs = MemberGroups
                .SelectMany(p => p.MemberDefinitions)
                .Where(p => p.Name.Equals(name, StringComparison.OrdinalIgnoreCase));

            foreach (var currentMemberDef in memberDefs)
            {
                var currentRelDef = currentMemberDef as RelationDefinition;

                if (currentRelDef != null && currentRelDef.Role == role)
                {
                    return currentRelDef;
                }
            }

            return null;
        }

        /// <summary>
        /// Starts tracking changes.
        /// </summary>
        public virtual void StartTracking()
        {
            IsTracking = true;
            MarkAsNonDirty();
            Labels.StartTracking();
            MemberGroups.StartTracking();

            foreach (var currentMemberGroup in MemberGroups)
            {
                currentMemberGroup.StartTracking();
            }
        }

        /// <summary>
        /// Marks this instance as existing.
        /// </summary>
        public void MarkAsExisting()
        {
            IsNew = false;

            foreach (var memberGroup in MemberGroups)
            {
                memberGroup.MarkAsExisting();
            }
        }

        /// <summary>
        /// Creates an empty instance of <see cref="Entity"/> for this definition.
        /// The type of the created instance will depend on the concrete type of the definition.
        /// </summary>
        /// <returns>An instance of <see cref="Entity"/> for this definition.</returns>
        public virtual Entity CreateEntityInst()
        {
            return new Entity();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Marks this instance as new.
        /// </summary>
        private void MarkAsNew()
        {
            IsNew = true;

            foreach (var memberGroup in MemberGroups)
            {
                memberGroup.MarkAsNew();
            }
        }

        /// <summary>
        /// If the entity-definition is tracking dirtyness, it is marked as dirty.
        /// If the entity-definition is not tracking, nothing happens.
        /// </summary>
        private void MarkAsDirty()
        {
            if (IsTracking)
                _isDirty = true;
        }

        /// <summary>
        /// Marks this instance as non dirty.
        /// </summary>
        private void MarkAsNonDirty()
        {
            _isDirty = false;
        }

        #endregion
    }
}
