﻿using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Stylelabs.Recruitment.Entities
{
    public class EntityFactory : IEntityFactory
    {
        #region Fields

        private readonly IPropertyFactory _propertyFactory;
        private readonly IRelationFactory _relationFactory;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="EntityFactory" /> class.
        /// </summary>
        /// <param name="propertyFactory">The property factory used to create properties from property-definitions.</param>
        /// <param name="relationFactory">The relation factory used to create relations from relation-definition.</param>
        public EntityFactory(IPropertyFactory propertyFactory, IRelationFactory relationFactory)
        {
            _propertyFactory = propertyFactory;
            _relationFactory = relationFactory;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Creates a new instance of <see cref="Entity" />.
        /// The entity will contains the properties defined in the specified <paramref name="definition" />.
        /// The values of the properties will be stored in the specified <paramref name="culture" />.
        /// </summary>
        /// <param name="definition">The definition to create an entity for.</param>
        /// <param name="culture">The culture to store the values of the properties in.</param>
        /// <param name="id"></param>
        /// <returns>
        /// A new instance of <see cref="Entity" />.
        /// </returns>
        /// <exception cref="System.ArgumentException">The specified definition does not have an Id. Make sure it has been stored in the database.</exception>
        public Entity Create(EntityDefinition definition, CultureInfo culture, long id)
        {
            Guard.ArgumentNotNull(definition, "definition");
            Guard.ArgumentNotNull(culture, "culture");

            //The definition must have been saved before it can be used to create entities.
            if (definition.Id <= 0)
            {
                throw new ArgumentException(
                    "The specified definition does not have an Id. Make sure it has been stored in the database.",
                    "definition");
            }

            //Create entity
            Entity entity = definition.CreateEntityInst();

            //Extract all members from the definition.
            var memberDefs = definition.MemberGroups.SelectMany(p => p.MemberDefinitions);

            var properties = new List<Property>();
            var relations = new List<Relation>();

            foreach (var currentMemberDef in memberDefs)
            {
                PropertyDefinition propDef = currentMemberDef as PropertyDefinition;
                if (propDef != null)
                {
                    properties.Add(_propertyFactory.Create(propDef));
                    continue;
                }

                RelationDefinition relDef = currentMemberDef as RelationDefinition;
                if (relDef != null)
                {
                    relations.Add(_relationFactory.Create(relDef));
                    continue;
                }

                throw new NotSupportedException("The specified implementation of MemberDefinition is not supported.");
            }

            //Set entity id
            entity.Id = id;
            
            //Set definition id
            entity.DefinitionId = definition.Id;
            entity.Definition = definition;

            //Create entity and return it.
            entity.Properties = new List<Property>(properties);
            entity.Relations = new List<Relation>(relations);
            entity.Culture = culture;

            //Start tracking
            entity.StartTracking();

            return entity;
        }

        /// <summary>
        /// Creates a new instance of <typeparamref name="T" />.
        /// The instance will contains the properties defined in the specified <paramref name="definition" />.
        /// The values of the properties will be stored in the specified <paramref name="culture" />.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="definition">The definition to create a <typeparamref name="T" /> for.</param>
        /// <param name="culture">The culture to store the values of the properties in.</param>
        /// <param name="id"></param>
        /// <returns>
        /// A new instance of <typeparamref name="T" />.
        /// </returns>
        /// <exception cref="System.InvalidOperationException">The specified generic type is not valid for the specified entity-definition.</exception>
        public T Create<T>(EntityDefinition definition, CultureInfo culture, long id) where T : Entity
        {
            Guard.ArgumentNotNull(definition, "definition");
            Guard.ArgumentNotNull(culture, "culture");


            var result = Create(definition, culture, id) as T;

            if (result == null)
                throw new InvalidOperationException("The specified generic type is not valid for the specified entity-definition.");

            return result;
        }

        #endregion
    }
}
