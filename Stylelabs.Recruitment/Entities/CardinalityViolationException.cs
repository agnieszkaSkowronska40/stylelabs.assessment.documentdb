﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Stylelabs.Recruitment.Entities
{
    public class CardinalityViolationException : Exception
    {
        public CardinalityViolationException(string message) : base(message) { }
    }
}
