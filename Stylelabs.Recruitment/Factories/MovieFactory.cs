﻿using System.Collections.Generic;
using System.Globalization;
using Stylelabs.Recruitment.Entities;

namespace Stylelabs.Recruitment.Factories
{
    public class MovieFactory
    {
        public static EntityDefinition Create(CultureInfo culture, long id)
        {
            
            var movieDefinition = new EntityDefinition
            {
                Id = id,
                Name = "Movie",
                Labels = { 
                        new KeyValuePair<CultureInfo, string>(culture, "Movie Definition")
                    },
                MemberGroups =
                    {
                        new MemberGroup
                        {
                            Name = "General",
                            MemberDefinitions =
                            {
                                new StringPropertyDefinition
                                {
                                    Name = "Title",
                                    IsMandatory = true,
                                    Labels = { { culture, "Title" } }
                                },
                                new DateTimePropertyDefinition
                                {
                                    Name = "ReleaseDate",
                                    IsMandatory = true,
                                    Labels = { { culture, "Release Date" } }
                                },
                                new StringPropertyDefinition
                                {
                                    Name = "StoryLine",
                                    Labels = { { culture, "StoryLine" } }
                                },

                                //Added one more definition for DateTime property
                                new DateTimePropertyDefinition
                                {
                                    Name = "CreatedDate",
                                    IsMandatory = false,
                                    Labels = { { culture, "Created Date" } }
                                },

                                //Added relation defintion
                                new RelationDefinition 
                                {
                                   Name = "MovieWith2Actors",
                                   ChildIsMandatory = true,
                                   Cardinality = RelationCardinality.OneToMany,
                                   Labels = { { culture, "Relation" } }
                                }
                            }
                         }
                    }
            };

            return movieDefinition;

        }
    }
}
