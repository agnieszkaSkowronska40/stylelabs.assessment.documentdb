﻿using System.Collections.Generic;
using System.Globalization;
using Stylelabs.Recruitment.Entities;

namespace Stylelabs.Recruitment.Factories
{
    public class ActorFactory
    {
        public static EntityDefinition Create(CultureInfo culture, long id)
        {
           
            var actorDefinition = new EntityDefinition
                {
                    Id = id,
                    Name = "Actor",
                    Labels = {  new KeyValuePair<CultureInfo, string>(culture, "Actor Definition") },
                    MemberGroups = 
                        {
                            new MemberGroup
                            {
                                Name = "General",
                                MemberDefinitions =
                                {
                                    new StringPropertyDefinition
                                    {
                                        Name = "Name",
                                        IsMandatory = true,
                                        Labels = { { culture, "Name" } }
                                    }
                                }
                            }
                        }
                };

            return actorDefinition;
        
        }
    }
}
