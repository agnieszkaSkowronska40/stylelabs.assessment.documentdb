﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// A change-tracking enabled list.
    /// </summary>
    /// <remarks>
    /// Always handle removed-items before new-items. Removing items that have been added results in a null-operation.
    /// Adding items that have been removed does not. This allows repositioning items.
    /// </remarks>
    /// <typeparam name="T">Generic type of the collection.</typeparam>
    public interface ITrackingList<T> : IList<T>, IDirtyTrackable
    {
        #region Properties

        /// <summary>
        /// Gets the items that have been removed since tracking has started.
        /// </summary>
        ICollection<T> RemovedItems { get; }

        /// <summary>
        /// Gets the items that have been added since tracking has started.
        /// </summary>
        ICollection<T> NewItems { get; }

        #endregion

        #region Events

        /// <summary>
        /// Occurs before an item is removed from the list.
        /// </summary>
        event EventHandler<TrackingListChangedEventArgs<T>> ItemRemoving;

        /// <summary>
        /// Occurs after an item has been removed from the list.
        /// </summary>
        event EventHandler<TrackingListChangedEventArgs<T>> ItemRemoved;

        /// <summary>
        /// Occurs before an item is inserted to the list.
        /// </summary>
        event EventHandler<TrackingListChangedEventArgs<T>> ItemInserting;

        /// <summary>
        /// Occurs after an item has been inserted to the list.
        /// </summary>
        event EventHandler<TrackingListChangedEventArgs<T>> ItemInserted;

        #endregion
    }
}
