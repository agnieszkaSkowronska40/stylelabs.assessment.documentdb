﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stylelabs.Recruitment.Entities;

namespace Stylelabs.Recruitment.Framework
{
    /// <summary>
    /// Default implementation of <see cref="ITrackingList{T}"/>.
    /// </summary>
    /// <typeparam name="T">Generic type of the list.</typeparam>
    public class TrackingList<T> : ITrackingList<T>
    {
        #region Fields

        private readonly IList<T> _innerList = new List<T>();

        #endregion

        #region Constructors

        public TrackingList()
        {
            RemovedItems = new HashSet<T>();
            NewItems = new HashSet<T>();
        }

        public TrackingList(IList<T> innerList)
            : this()
        {
            _innerList = innerList;
        }

        #endregion

        #region Events

        /// <summary>
        /// Occurs before an item is removed from the list.
        /// </summary>
        public event EventHandler<TrackingListChangedEventArgs<T>> ItemRemoving;

        /// <summary>
        /// Occurs after an item has been removed from the list.
        /// </summary>
        public event EventHandler<TrackingListChangedEventArgs<T>> ItemRemoved;

        /// <summary>
        /// Occurs before an item is inserted to the list.
        /// </summary>
        public event EventHandler<TrackingListChangedEventArgs<T>> ItemInserting;

        /// <summary>
        /// Occurs after an item has been inserted to the list.
        /// </summary>
        public event EventHandler<TrackingListChangedEventArgs<T>> ItemInserted;

        #endregion

        #region Protected methods

        /// <summary>
        /// Called after an item has been inserted into the list.
        /// </summary>
        /// <param name="item">The item that has been inserted.</param>
        protected virtual void OnItemInserted(T item)
        {
            var evt = ItemInserted;
            if (evt != null)
            {
                evt(this, new TrackingListChangedEventArgs<T>(item));
            }

            if (IsTracking)
            {
                NewItems.Add(item);
            }
        }

        /// <summary>
        /// Called before an item is inserted into the list.
        /// </summary>
        /// <param name="item">The item that will be inserted.</param>
        protected virtual void OnItemInserting(T item)
        {
            var evt = ItemInserting;
            if (evt != null)
            {
                evt(this, new TrackingListChangedEventArgs<T>(item));
            }
        }

        /// <summary>
        /// Called when after item has been removed from the collection.
        /// </summary>
        /// <param name="item">The item that has been removed.</param>
        protected virtual void OnItemRemoved(T item)
        {
            var evt = ItemRemoved;
            if (evt != null)
            {
                evt(this, new TrackingListChangedEventArgs<T>(item));
            }

            if (IsTracking)
            {
                if (NewItems.Contains(item))
                {
                    NewItems.Remove(item);
                }
                else
                {
                    RemovedItems.Add(item);
                }
            }
        }

        /// <summary>
        /// Called before an item is removed from the list.
        /// </summary>
        /// <param name="item">The item that will be removed.</param>
        protected virtual void OnItemRemoving(T item)
        {
            var evt = ItemRemoving;
            if (evt != null)
            {
                evt(this, new TrackingListChangedEventArgs<T>(item));
            }
        }

        #endregion

        #region Implementation of ITrackingList

        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        public bool IsTracking { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public bool IsDirty
        {
            get { return NewItems.Count > 0 || RemovedItems.Count > 0; }
        }

        /// <summary>
        /// Gets the items that have been removed since tracking has started.
        /// </summary>
        public ICollection<T> RemovedItems { get; private set; }

        /// <summary>
        /// Gets the items that have been added since tracking has started.
        /// </summary>
        public ICollection<T> NewItems { get; private set; }

        /// <summary>
        /// Starts tracking changes made to the list.
        /// </summary>
        public void StartTracking()
        {
            IsTracking = true;

            NewItems.Clear();
            RemovedItems.Clear();
        }

        #endregion

        #region Implementation of IList

        public int IndexOf(T item)
        {
            return _innerList.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            OnItemInserting(item);
            _innerList.Insert(index, item);
            OnItemInserted(item);
        }

        public void RemoveAt(int index)
        {
            var item = _innerList[index];
            OnItemRemoving(item);
            _innerList.RemoveAt(index);
            OnItemRemoved(item);
        }

        public T this[int index]
        {
            get { return _innerList[index]; }
            set
            {
                var oldItem = _innerList[index];

                OnItemRemoving(oldItem);
                OnItemInserting(value);
                _innerList[index] = value;
                OnItemRemoved(oldItem);
                OnItemInserted(value);
            }
        }

        public void Add(T item)
        {
            OnItemInserting(item);
            _innerList.Add(item);
            OnItemInserted(item);
        }

        public void Clear()
        {
            var itemsToRemove = _innerList.ToArray(); //Copy current

            foreach (var current in itemsToRemove)
            {
                OnItemRemoving(current);
            }

            _innerList.Clear();

            foreach (var current in itemsToRemove)
            {
                OnItemRemoved(current);
            }
        }

        public bool Contains(T item)
        {
            return _innerList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _innerList.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _innerList.Count; }
        }

        public bool IsReadOnly
        {
            get { return _innerList.IsReadOnly; }
        }

        public bool Remove(T item)
        {
            OnItemRemoving(item);

            bool result = _innerList.Remove(item);

            if (result)
            {
                OnItemRemoved(item);
            }

            return result;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _innerList.GetEnumerator();
        }

        #endregion
    }
}
