﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public class TrackingListChangedEventArgs<T> : EventArgs
    {
        #region Properties

        public T Item { get; private set; }

        #endregion

        #region Constructors

        public TrackingListChangedEventArgs(T item)
        {
            Item = item;
        }

        #endregion
    }
}
