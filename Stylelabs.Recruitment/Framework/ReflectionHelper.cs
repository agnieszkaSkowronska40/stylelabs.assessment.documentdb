﻿using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public static class ReflectionHelper
    {
        /// <summary>
        /// Creates an instance of the specified type.
        /// </summary>
        /// <param name="type">The type to create an instance of.</param>
        /// <returns></returns>
        public static object CreateInstance(Type type)
        {
            Guard.ArgumentNotNull(type, "type");

            return Activator.CreateInstance(type);
        }

        /// <summary>
        /// Creates an instance of a generic type.
        /// </summary>
        /// <param name="type">The generic type.</param>
        /// <param name="genericArgument">The generic type argument.</param>
        /// <returns></returns>
        public static object CreateGenericInstance(Type type, Type genericArgument)
        {
            Guard.ArgumentNotNull(type, "type");
            Guard.ArgumentNotNull(genericArgument, "genericArgument");

            var gType = type.MakeGenericType(genericArgument);
            return CreateInstance(gType);
        }

        /// <summary>
        /// Gets the name of the specified type. The name includes the name of the containing assembly, but without versioning information.
        /// </summary>
        /// <param name="type">The type to get the name for.</param>
        /// <returns>The name of the specified type including the assembly, but without versioning information.</returns>
        public static string GetTypeNameWithAssembly(Type type)
        {
            Guard.ArgumentNotNull(type, "type");

            return type.FullName + "," + type.Assembly.GetName().Name;
        }
    }
}
