﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylelabs.Recruitment.Framework
{
    public class TrackingDictionary<TKey, TValue> : ITrackingDictionary<TKey, TValue>
    {
        #region Fields

        private readonly IDictionary<TKey, TValue> _innerDictionary = new Dictionary<TKey, TValue>();

        #endregion

        #region Constructors

        public TrackingDictionary()
        {
        }

        public TrackingDictionary(IDictionary<TKey, TValue> innerDictionary)
            : this()
        {
            _innerDictionary = innerDictionary;
        }

        #endregion

        #region Implementation of ITrackingList

        /// <summary>
        /// Gets a value indicating whether this instance is tracking.
        /// </summary>
        public bool IsTracking { get; protected set; }

        /// <summary>
        /// Gets a value indicating whether this instance is dirty.
        /// </summary>
        public bool IsDirty { get; private set; }

        /// <summary>
        /// Starts tracking changes made to the list.
        /// </summary>
        public void StartTracking()
        {
            IsDirty = false;
            IsTracking = true;
        }

        #endregion

        #region Implementation of IDictionary<TKey, TValue>

        public void Add(TKey key, TValue value)
        {
            var kvp = new KeyValuePair<TKey, TValue>(key, value);

            _innerDictionary.Add(kvp);

            if (IsTracking) IsDirty = true;
        }

        public bool ContainsKey(TKey key)
        {
            return _innerDictionary.ContainsKey(key);
        }

        public ICollection<TKey> Keys
        {
            get { return _innerDictionary.Keys; }
        }

        public bool Remove(TKey key)
        {
            if (_innerDictionary.ContainsKey(key))
            {
                var result = _innerDictionary.Remove(key);

                if (result && IsTracking) IsDirty = true;

                return result;
            }
            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _innerDictionary.TryGetValue(key, out value);
        }

        public ICollection<TValue> Values
        {
            get { return _innerDictionary.Values; }
        }

        public TValue this[TKey key]
        {
            get { return _innerDictionary[key]; }
            set
            {
                _innerDictionary[key] = value;
                if (IsTracking) IsDirty = true;
            }
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            _innerDictionary.Add(item);
            if (IsTracking) IsDirty = true;
        }

        public void Clear()
        {
            _innerDictionary.Clear();
            if (IsTracking) IsDirty = true;
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _innerDictionary.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _innerDictionary.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _innerDictionary.Count; }
        }

        public bool IsReadOnly
        {
            get { return _innerDictionary.IsReadOnly; }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            if (_innerDictionary.Remove(item))
            {
                if (IsTracking) IsDirty = true;
                return true;
            }
            return false;
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _innerDictionary.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _innerDictionary.GetEnumerator();
        }

        #endregion
    }
}
