﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Stylelabs.Recruitment.Entities;

namespace Stylelabs.Recruitment.Framework.JSONConverters
{
    public class GenericConverter<T> : JsonConverter 
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(T);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (typeof(T) == typeof(List<Relation>))
            {
               object result = (serializer.Deserialize(reader, typeof(List<ChildToManyParentsRelation>)) ??
                         serializer.Deserialize(reader, typeof(List<ChildToOneParentRelation>))) ??
                         serializer.Deserialize(reader, typeof(List<ParentToManyChildrenRelation>)) ??
                         serializer.Deserialize(reader, typeof(List<ParentToOneChildRelation>));
               return result;
            }

            if (typeof(T) == typeof(List<Property>))
            {
                object result = (serializer.Deserialize(reader, typeof (List<Property<string>>)) ??
                                 serializer.Deserialize(reader, typeof (List<Property<DateTime>>)));
               return result;
            }
            return serializer.Deserialize<T>(reader);
        }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            serializer.Serialize(writer, value);
        }
    }
}
