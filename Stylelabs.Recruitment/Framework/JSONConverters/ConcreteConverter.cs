﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Stylelabs.Recruitment.Entities;

namespace Stylelabs.Recruitment.Framework.JSONConverters
{
    public class ConcreteConverter<T> : JsonConverter    
    {
        public override bool CanConvert(Type objectType)
        {
            return (typeof(T) == typeof(TrackingList<MemberDefinition>));
        }
    
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (typeof (T) == typeof (TrackingList<MemberDefinition>))
            {
               object result = (serializer.Deserialize(reader, typeof (TrackingList<StringPropertyDefinition>)) ??
                                serializer.Deserialize(reader, typeof (TrackingList<RelationDefinition>))) ??
                                serializer.Deserialize(reader, typeof (TrackingList<DateTimePropertyDefinition>));

                return ConvertToITrackingList(result);
            }

            return serializer.Deserialize<T>(reader);
        }

        private static TrackingList<MemberDefinition> ConvertToITrackingList(object item)
        {
            TrackingList<MemberDefinition> finalList = new TrackingList<MemberDefinition>();
            IEnumerable<MemberDefinition> newList = GetMemberDefinitions(item);

            if (newList == null) 
                return finalList;
            
            foreach (MemberDefinition element in newList)
                finalList.Add(element);

            return finalList;
        }

        private static IEnumerable<MemberDefinition> GetMemberDefinitions(object item)
        {
            IEnumerable<MemberDefinition> newList = null;
            var itemType = item.GetType();

            if (typeof (TrackingList<StringPropertyDefinition>) == itemType)
                newList = (IEnumerable<StringPropertyDefinition>) item;

            if (typeof (TrackingList<DateTimePropertyDefinition>) == itemType)
                newList = (IEnumerable<DateTimePropertyDefinition>) item;

            if (typeof (TrackingList<RelationDefinition>) == itemType)
                newList = (IEnumerable<DateTimePropertyDefinition>) item;
            return newList;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)    
        {    
            serializer.Serialize(writer, value);    
        }
    }    
}
