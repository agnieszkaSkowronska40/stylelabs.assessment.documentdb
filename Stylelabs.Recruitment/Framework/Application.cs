﻿using System.Globalization;
using Autofac;
using Stylelabs.Recruitment.Entities;
using Stylelabs.Recruitment.Factories;

namespace Stylelabs.Recruitment.Framework
{
    public static class Application
    {
        // Get default Culture
        private static readonly CultureInfo Culture = CultureInfo.GetCultureInfo("en-US");
        
        public static IContainer Container { get; private set; }

        public static void Startup()
        {
            //Create new autofac container-builder.
            var containerBuilder = new ContainerBuilder();

            //Register all types
            containerBuilder.RegisterType<EntityFactory>().As<IEntityFactory>();
            containerBuilder.RegisterType<PropertyFactory>().As<IPropertyFactory>();
            containerBuilder.RegisterType<RelationFactory>().As<IRelationFactory>();

            var actorDefinition = ActorFactory.Create(Culture, 1);
            var movieDefinition = MovieFactory.Create(Culture, 2);

            containerBuilder.RegisterInstance(actorDefinition).Keyed<EntityDefinition>("ActorDef");
            containerBuilder.RegisterInstance(movieDefinition).Keyed<EntityDefinition>("MovieDef");

            Container = containerBuilder.Build();
        }
    }
}
