﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;

namespace Stylelabs.Providers
{
    public class DocumentDbRepository<T> : IDocumentDbRepository<T>
    {
        private static string _databaseId;
        private static string _endpoint; 
        private static string _authKey;

        private static DocumentClient _client;
        private static DocumentCollection _collection;
        public DocumentDbRepository()
        {
            ReadConfiguration();
        }

        private static void ReadConfiguration()
        {
           _databaseId = ConfigurationManager.AppSettings["database"];
           _endpoint = ConfigurationManager.AppSettings["endpoint"];
           _authKey = ConfigurationManager.AppSettings["authKey"];

            if(_databaseId == null)
                _databaseId = "test1";
            if (_endpoint == null)
            _endpoint = "https://askowronska40.documents.azure.com:443/";
            if(_authKey == null)
            _authKey = "Q4CVqd5uUEIO0QReaRUdDgXTQypkzpxBBd0bb1cZS6fOwpW5Pe2W9k/CKHJhXz/lfgZXKtqYVkr13kri4gnJhQ==";
            
        }
        
        public void SetCollection(string collectionId)
        {
           var database = ReadOrCreateDatabase();
           _collection = ReadOrCreateCollection(database.SelfLink, collectionId);
        }

        public IEnumerable<T> GetAllItems()
        {
            var client = Client();
            return client.CreateDocumentQuery<T>(_collection.DocumentsLink)
                .AsEnumerable();
        }

        public IEnumerable<T> GetItems(Expression<Func<T, bool>> predicate)
        {
            var client = Client();
            return client.CreateDocumentQuery<T>(_collection.DocumentsLink)
                .Where(predicate)
                .AsEnumerable();
        }


        public void CreateItem(T item)
        {
            var client = Client();
            Task result = client.CreateDocumentAsync(_collection.SelfLink, item);
            Task.WaitAll(result);
        }

        public bool UpdateItem(Expression<Func<T, bool>> predicate, T item)
        {
           var client = Client();
           dynamic doc = client.CreateDocumentQuery<T>(_collection.DocumentsLink).Where(predicate).AsEnumerable().FirstOrDefault();
            if (doc == null)
               return false;
            
           Task result = client.ReplaceDocumentAsync(doc.SelfLink, item);
           Task.WaitAll(result);
           return true;
        }

        public bool DeleteItem(Expression<Func<T, bool>> predicate)
        {
            var client = Client();
            dynamic doc = client.CreateDocumentQuery<T>(_collection.DocumentsLink).Where(predicate).AsEnumerable().FirstOrDefault();
            if (doc == null)
                return false;
           
           Task result = client.DeleteDocumentAsync(doc.SelfLink);
           Task.WaitAll(result);
           return true;
        }

        public void Dispose()
        {
            if (_client != null)
                _client.Dispose();
        }

        private static DocumentClient Client()
        {
            if (_client != null)
                return _client;

            var endpointUri = new Uri(_endpoint);
            _client = new DocumentClient(endpointUri, _authKey);
            return _client;
        }

        private static Database ReadOrCreateDatabase()
        {
            var client = Client();
            var db = client.CreateDatabaseQuery()
                .Where(d => d.Id == _databaseId)
                .AsEnumerable()
                .FirstOrDefault();

            if (db == null)
               db = client.CreateDatabaseAsync(new Database {Id = _databaseId}).Result;
            
            return db;
        }

        private static DocumentCollection ReadOrCreateCollection(string databaseLink, string collectionId)
        {
            var client = Client();
            var col = client.CreateDocumentCollectionQuery(databaseLink)
                              .Where(c => c.Id == collectionId)
                              .AsEnumerable()
                              .FirstOrDefault();

            if (col != null) return col;

            var collectionSpec = new DocumentCollection { Id = collectionId };
            var requestOptions = new RequestOptions { OfferType = "New" };
          
            col = client.CreateDocumentCollectionAsync(databaseLink, collectionSpec, requestOptions).Result;

            return col;
        }

        private static Document GetDocument(string id)
        {
           var client = Client();
           return client.CreateDocumentQuery(_collection.DocumentsLink)
                .Where(x => x.Id == id)
                .AsEnumerable()
                .FirstOrDefault();
        }

    }
}
