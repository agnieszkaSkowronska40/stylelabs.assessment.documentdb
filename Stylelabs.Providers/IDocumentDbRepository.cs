﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Stylelabs.Providers
{
    public interface IDocumentDbRepository<T> : IDisposable
    {
        void SetCollection(string collectionId);
        IEnumerable<T> GetAllItems();
        IEnumerable<T> GetItems(Expression<Func<T, bool>> predicate);
        void CreateItem(T item);
        bool UpdateItem(Expression<Func<T, bool>> predicate, T item);
        bool DeleteItem(Expression<Func<T, bool>> predicate);
    }
}
