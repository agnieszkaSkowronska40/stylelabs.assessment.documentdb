﻿using Stylelabs.Recruitment.Framework;
using System;
using System.Configuration;
using Autofac;
using Stylelabs.Recruitment.Entities;
using System.Globalization;
using System.Linq;
using Stylelabs.DataAccess.Client;
using Stylelabs.Repositories;

namespace Stylelabs.Recruitment.App
{
    internal enum DefinitionsId : long
    {
       Actor = 1, Movie = 2
    }

    internal enum EntitiesId : long
    {
        Actor1 = 100, Actor2 = 200, Movie = 500
    }

    public class Program
    {
        private static bool _isRestServiceAvailable;
        private static IRepository<Entity> _entityRepo;
        private static IRepository<EntityDefinition> _definitionRepo;
        private static readonly IEntitiesClient EntitiesClient = new EntitiesClient();
        private static readonly CultureInfo Culture = CultureInfo.GetCultureInfo("en-US");

        static void Main(string[] args)
        {
            _isRestServiceAvailable = false;
            
            // Wire classes
            Application.Startup();

            if (!_isRestServiceAvailable)
               SetEntityDefRepository();

            CreateEntitiesDefinitions();

            SetEntities();

            //Get entity from the DocumentDB NoSQL
            Entity movie = GetEntityFromDatabase((long)EntitiesId.Movie);

            _definitionRepo.Dispose();
            _entityRepo.Dispose();
            EntitiesClient.Dispose();
        }

        private static void SetEntities()
        {
            //Get from Autofac IoC EntityFactory object where IoC automaticaly 
            //inject PropertyFactory and RelationFactory object into constructor (autofactory of Autofac)
            var entityFactory = Application.Container.Resolve<IEntityFactory>();

            //Get entity definitions from the DocumentDB NoSQL
            var movieDef = GetDefinitionFromDatabase((long)DefinitionsId.Movie);
            var actorDef = GetDefinitionFromDatabase((long)DefinitionsId.Actor);

            var actor1 = entityFactory.Create(actorDef, Culture, (long)EntitiesId.Actor1);
            actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 1");

            var actor2 = entityFactory.Create(actorDef, Culture, (long)EntitiesId.Actor2);
            actor2.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 2");

            // add to DocumentDB
            AddEntityToDatabase(actor1);
            AddEntityToDatabase(actor2);

            var movie = entityFactory.Create(movieDef, Culture, (long)EntitiesId.Movie);
            movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
            movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(DateTime.Now);
            movie.GetProperty<DateTime>("CreatedDate").Value = new PropertyValue<DateTime>(DateTime.UtcNow);

            var relation = movie.GetRelation<Relation>("MovieWith2Actors");
            relation.SetLinks(actor1.Id, actor2.Id);
            movie.AddRelation(relation);
            
            // add to DocumentDB
           AddEntityToDatabase(movie);
        }

        private static void AddEntityToDatabase(Entity entity)
        {
            if (_isRestServiceAvailable)
                EntitiesClient.PostEntity(entity);
            else
            _entityRepo.CreateItemAsync(entity);
        }


        private static EntityDefinition GetDefinitionFromDatabase(long id)
        {
            return _isRestServiceAvailable ? EntitiesClient.GetEntityDefinitionById(id) : _definitionRepo.GetItems(d => d.Id == id).FirstOrDefault();
        }

        private static Entity GetEntityFromDatabase(long id)
        {
            return _isRestServiceAvailable ? EntitiesClient.GetEntityById(id) : _entityRepo.GetItems(d => d.Id == id).FirstOrDefault();
        }

        private static void SetEntityDefRepository()
        {
            _definitionRepo = new Repository<EntityDefinition>();
            _definitionRepo.SetCollection(ConfigurationManager.AppSettings["entityDefCollection"]);

            _entityRepo = new Repository<Entity>();
            _entityRepo.SetCollection(ConfigurationManager.AppSettings["entityCollection"]);
        }

        private static void CreateEntitiesDefinitions()
        {
            // Create domain model consisting of a movie and actor definition
            var movieDefinition = Application.Container.ResolveKeyed<EntityDefinition>("ActorDef");
            var actorDefinition = Application.Container.ResolveKeyed<EntityDefinition>("MovieDef");

            if (_isRestServiceAvailable)
            {
                // Add definitions into DocumentDB via service
                EntitiesClient.PostEntityDefinition(actorDefinition);
                EntitiesClient.PostEntityDefinition(movieDefinition);
            }
            else
            {
                // Add definitions into DocumentDB
                _definitionRepo.CreateItemAsync(actorDefinition);
                _definitionRepo.CreateItemAsync(movieDefinition);
            }

        }

    }
}


