﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Stylelabs.DataAccess.Client.Properties;
using Stylelabs.Recruitment.Entities;
using Newtonsoft.Json.Linq;

namespace Stylelabs.DataAccess.Client
{
    public class EntitiesClient : IEntitiesClient
    {
        private static HttpClient _client;
        public EntitiesClient()
        {
            _client = new HttpClient();
        }
         
        public IEnumerable<Entity> GetAllEntities()
        {
            var url = GetUrl("/api/entity");
            HttpResponseMessage response = _client.GetAsync(url).Result;
            var result = (response.IsSuccessStatusCode) ? new List<Entity>() : response.Content.ReadAsAsync<JArray>().Result.ToObject<IList<Entity>>();
            return result;
        }

        public IEnumerable<EntityDefinition> GetAllEntityDefinitions()
        {
            var url = GetUrl("/api/entitydefinition");
            HttpResponseMessage response = _client.GetAsync(url).Result;
            return !response.IsSuccessStatusCode ? new List<EntityDefinition>() : response.Content.ReadAsAsync<JArray>().Result.ToObject<IList<EntityDefinition>>();
        }

        public Entity GetEntityById(long id)
        {
            var url = GetUrl(String.Format("/api/entity/{0}", id));
            HttpResponseMessage response = _client.GetAsync(url).Result;
            return !response.IsSuccessStatusCode ? null: response.Content.ReadAsAsync<JObject>().Result.ToObject<Entity>();
        }

        public EntityDefinition GetEntityDefinitionById(long id)
        {
            var url = GetUrl(String.Format("/api/entitydefinition/{0}", id));
            HttpResponseMessage response = _client.GetAsync(url).Result;
            return !response.IsSuccessStatusCode ? null: response.Content.ReadAsAsync<JObject>().Result.ToObject<EntityDefinition>();
        }

        public bool PostEntity(Entity entity)
        {
            var url = GetUrl("/api/entity");
            Task<HttpResponseMessage> response = _client.PostAsJsonAsync(url, entity);
            Task.WaitAll(response);
            return response.Result.IsSuccessStatusCode;
        }

        public bool PostEntityDefinition(EntityDefinition entityDef)
        {
            var url = GetUrl("/api/entitydefinition");
            Task<HttpResponseMessage> response = _client.PostAsJsonAsync(url, entityDef);
            Task.WaitAll(response);
            return response.Result.IsSuccessStatusCode;
        }

        public bool PutEntity(Entity entity)
        {
           var url = GetUrl("/api/entity");
           Task<HttpResponseMessage> response = _client.PutAsJsonAsync(url, entity);
           Task.WaitAll(response);
           return response.Result.IsSuccessStatusCode;
        }

        public bool PutEntityDefinition(EntityDefinition entityDef)
        {
            var url = GetUrl("/api/entitydefinition");
            Task<HttpResponseMessage> response = _client.PutAsJsonAsync(url, entityDef);
            Task.WaitAll(response);
            return response.Result.IsSuccessStatusCode;
        }

        public bool DeleteEntity(long id)
        {
            var url = GetUrl(string.Format("api/entity/{0}", id));
            Task<HttpResponseMessage>response = _client.DeleteAsync(url);
            Task.WaitAll(response);
            return response.Result.IsSuccessStatusCode;
        }
        public bool DeleteEntityDefinition(long id)
        {
            var url = string.Format("api/entitydefinition/{0}", id);
            Task<HttpResponseMessage> response = _client.DeleteAsync(url);
            Task.WaitAll(response);
            return response.Result.IsSuccessStatusCode;
        }


        public void Dispose()
        {
            if(_client != null)
                _client.Dispose();
        }

        private static Uri GetUrl(string path)
        {
            Uri baseUri = new Uri(Resources.DeployPath);
            Uri currentUri = new Uri(baseUri, path);
            return currentUri;
        }
    }
}
