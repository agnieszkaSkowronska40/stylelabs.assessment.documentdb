﻿using System;
using System.Collections.Generic;
using Stylelabs.Recruitment.Entities;

namespace Stylelabs.DataAccess.Client
{
    public interface IEntitiesClient : IDisposable
    {
       IEnumerable<Entity> GetAllEntities();
       IEnumerable<EntityDefinition> GetAllEntityDefinitions();
       Entity GetEntityById(long id);
       EntityDefinition GetEntityDefinitionById(long id);
       bool PostEntity(Entity entity);
       bool PostEntityDefinition(EntityDefinition entityDef);
       bool PutEntity(Entity entity);
       bool PutEntityDefinition(EntityDefinition entityDef);
       bool DeleteEntity(long id);
       bool DeleteEntityDefinition(long id);
    }
}
