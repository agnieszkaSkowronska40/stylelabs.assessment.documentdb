﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Stylelabs.Recruitment.Entities;
using Stylelabs.Repositories;

namespace Stylelabs.DataAccess.Service.Controllers
{
    public class EntityController : ApiController
    {
        private const string CollectionName = "EntityCollection";
        private static readonly IRepository<Entity> EntityRepo = new Repository<Entity>();

        public EntityController()
        {
            EntityRepo.SetCollection(CollectionName);
        }

        // GET: api/entity
        public IEnumerable<Entity> GetAllEntities()
        {
            return EntityRepo.GetAllItems();
        }

        // GET: api/entity/id
        public Entity GetEntityById(long id)
        {
           Entity entity = EntityRepo.GetItem(item => item.Id == id);
           
            if (entity == null)
              throw new HttpResponseException(HttpStatusCode.NotFound);
           return entity;
        }

        // POST: api/entity
        public void PostEntity(Entity entity)
        {
           EntityRepo.CreateItemAsync(entity);
        }

        // PUT: api/entity/id
        public bool PutEntity(Entity entity, long id)
        {
           return EntityRepo.UpdateItemAsync(item => item.Id == id, entity);
        }

        // DELETE: api/entity/id
        public bool DeletePerson(long id)
        {
           return EntityRepo.DeleteItem(item => item.Id == id);
        }
    }

  }
