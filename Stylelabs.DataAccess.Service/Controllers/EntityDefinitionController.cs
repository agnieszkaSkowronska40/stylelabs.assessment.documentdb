﻿using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Stylelabs.Recruitment.Entities;
using Stylelabs.Repositories;

namespace Stylelabs.DataAccess.Service.Controllers
{
    public class EntityDefinitionController : ApiController
    {
        private const string CollectionName = "EntityDefCollection";
        private static readonly IRepository<EntityDefinition> EntityDefinitionRepo = new Repository<EntityDefinition>();

        public EntityDefinitionController()
        {
            EntityDefinitionRepo.SetCollection(CollectionName);
        }

        // GET: api/EntityDefinition
        public IEnumerable<EntityDefinition> GetAllEntities()
        {
            return EntityDefinitionRepo.GetAllItems();
        }

        // GET: api/EntityDefinition/id
        public EntityDefinition GetEntityById(long id)
        {
           EntityDefinition entity = EntityDefinitionRepo.GetItem(item => item.Id == id);
           if (entity == null)
              throw new HttpResponseException(HttpStatusCode.NotFound);
           return entity;
        }

        // POST: api/EntityDefinition
        public void PostEntity(EntityDefinition entity)
        {
           EntityDefinitionRepo.CreateItemAsync(entity);
        }

        // PUT: api/EntityDefinition/id
        public bool PutEntity(EntityDefinition entity, long id)
        {
           return EntityDefinitionRepo.UpdateItemAsync(item => item.Id == id, entity);
        }

        // DELETE: api/EntityDefinition/id
        public bool DeletePerson(long id)
        {
           return EntityDefinitionRepo.DeleteItem(item => item.Id == id);
        }
    }
  }
