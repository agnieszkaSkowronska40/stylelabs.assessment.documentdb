﻿using System;
using System.Globalization;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Stylelabs.Recruitment.Entities;
using Stylelabs.Recruitment.Factories;
using Stylelabs.Recruitment.Framework;
using FluentAssertions;

namespace Stylelabs.Repositories.Integration.Tests
{
    [TestClass]
    public class RepositoryTest
    {
        // Get default Culture
        private static readonly CultureInfo Culture = CultureInfo.GetCultureInfo("en-US");

        [TestInitialize]
        public void Init()
        {
            Application.Startup();
        }


        [TestMethod]
        public void CreateEntityObjectsAndGetFromDb()
        {
           //When
           var movieDefinition = MovieFactory.Create(Culture, 1);
           var actorDefinition = ActorFactory.Create(Culture, 2);
            
           IRepository<EntityDefinition> definitionRepo = new Repository<EntityDefinition>();
           definitionRepo.SetCollection("EntityDefTestCollection");
            
           definitionRepo.CreateItemAsync(actorDefinition);
           definitionRepo.CreateItemAsync(movieDefinition);

           //Then
           var result = definitionRepo.GetItems(d => d.Id == 2).FirstOrDefault();

           //Check
            result.Should().NotBeNull();
            result.Should().BeOfType<EntityDefinition>();
            
            result.Id.ShouldBeEquivalentTo(2);
            result.Labels.Keys.Should().Equal(Culture);
            result.Labels.Values.Should().Equal("Actor Definition");
            //result.MemberGroups.Should().NotBeNull();
        }

        [TestMethod]
        public void CreateEntityDefinitionObjectsAndGetFromDb()
        {
           //When
           var movieDefinition = MovieFactory.Create(Culture, 1);
           var actorDefinition = ActorFactory.Create(Culture, 2);
            
           IRepository<Entity> entityRepo = new Repository<Entity>();
           entityRepo.SetCollection("EntityTestCollection");

           var entityFactory = Application.Container.Resolve<IEntityFactory>();
           
           var actor1 = entityFactory.Create(actorDefinition, Culture, 2);
           actor1.GetProperty<string>("Name").Value = new PropertyValue<string>("Actor 1");

           var movie = entityFactory.Create(movieDefinition, Culture, 1);
           movie.GetProperty<string>("Title").Value = new PropertyValue<string>("My Movie");
           movie.GetProperty<DateTime>("ReleaseDate").Value = new PropertyValue<DateTime>(DateTime.Now);
           movie.GetProperty<DateTime>("CreatedDate").Value = new PropertyValue<DateTime>(DateTime.UtcNow);

           var relation = movie.GetRelation<Relation>("MovieWith2Actors");
           relation.SetLinks(actor1.Id);
           movie.AddRelation(relation);

          
           entityRepo.CreateItemAsync(movie);
            
           //Then
           var result = entityRepo.GetItems(d => d.Id == 1).FirstOrDefault();
           
           //Check 
           result.Should().NotBeNull();
           result.Should().BeOfType<Entity>();
           result.Id.ShouldBeEquivalentTo(1);
           
           var properties = result.Properties.FirstOrDefault();
           properties.Should().NotBeNull();
           properties.Name.Should().NotBeEmpty();
           properties.Value.Should().NotBeNull();

           var relations = result.Relations.FirstOrDefault();
           relations.Should().NotBeNull();
        }

    }
}
